<?php

$browser = array("Firefox",
                 "Chrome",
                 "Internet Explorer",
                 "Safari",
                 "Opera",
                 "Other"
                );

class Select {
    public $name;
    public $value;


    public function setName($name) {
        $this -> name = $name;
    }

    public function getName() {
        return $this -> name;
    }

    public function setValue($value) {
        if (!is_array($value)) {
            exit ("Error: value is not array");
        }
        $this -> value = $value;
    }

    public function getValue() {
        return $this -> value;
    }

    public function makeOption($value) {
        foreach ($value as $val) {
            echo "<option value = \"$val\">" . strtoupper ($val) . "</option>\n";        }
    }

    public function makeSelect() {
        echo "<select name=\"" . $this -> getName() . "\">\n";
        $this -> makeOption ($this -> getValue());
        echo "</select>";
    }
}

?>

<html>
<head>
<title>REGISTRATION FORM</title>
</head>

<?php

    if(!isset($_POST['submit'])) {

?>

<form method="post">
<table>
<tr>
<th>
Name:
<input type="text" name="name"><br>
</th>
</tr>
<tr>
<th>
Username:
<input type="text" name="username"><br>
</th>
</tr>
<tr>
<th>
Email:
<input type="text" name="email"><br>
</th>
</tr>
<tr>
<th>
Browser:
<?php 

    $browse = new Select();

    $browse -> setName('WEB');
    $browse -> setValue($browser);
    $browse -> makeSelect();
?>
</th>
</tr>
    </table>
<input type="submit" name="submit">
</form>

<?php  

    } else {
        $name = $_POST['name'];
        $username = $_POST['username'];
        $email = $_POST['email'];
        $selectbrowser = $_POST['browse'];

        echo "The data has been saved for " . $name . "<br>";
        echo "Username: " . $username . "<br>";
        echo "Email: " . $email . "<br>";
        echo "Browser: " . $selectbrowser . "<br>";
        ?>
         <p><a href="/chapter6/ex1.php" class="button block">&laquo; Go to Home Page</a></p>
         <?php
    }
    ?>


    </body>
    </html>